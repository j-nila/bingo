# Bingo

Bingo - A simple bingo game to play with students as an ice-breaking activity

## Description
The application reads a file named `answers.json` that must be placed in the `src/data` folder. This file contains all the answers to all the possible bingo questions which are used to randomly create the Bingo cards. 

TODO: The JSON object contains an array of questions and another array of possible answers. Note that there is not necessarily any correlation between the two arrays (see examples below).

Some suggestions for use in an ice-breaking activity are:

* The teacher calls the answer names randomly. The winner is the first student to complete a line with the names of the other students in the class **and** who is able to indicate which name matches which student
* TODO: the answers are the students' names, and questions might be related to possible hobbies, pets, neighbourhoods, etc. The winner is the first student to complete a line with the names of the other students in the class **and** who is able to indicate which student matches which name and **how** they said they met the question (e.g. "John likes playing football", "Mary has a dog", etc.). It is possible that no student matches a question (e.g., no one lives in Laval).

## Visuals
TODO

## Technology Stack

[![React](https://img.shields.io/badge/react-grey?logo=react)](https://reactjs.org/)
[![TypeScript](https://img.shields.io/badge/typescript-blue?logo=typescript&logoColor=white)](https://www.typescriptlang.org/)
[![Vite](https://img.shields.io/badge/vite-white?logo=vite)](https://vitejs.dev/)


## Getting started

### Development:

```bash
cd bingo
npm i
npm run dev
```

Open the browser at http://localhost:5173

### Production:
  
```bash
cd bingo
npm i
npm run build
```

The `dist` folder contains the production files. Copy them to the web server.

## Installation
GitLab CI/CD is used to deploy the application to GitLab Pages. At this time it is a client application only. Under Deploy -> Pages, **uncheck** "Use unique domain".



## Roadmap

* multiple cards per game
* validation that the boxes checked were indeed called (requires a backend, and a calling mechanism)
* multiple rooms with different cards

## Contributing
TODO: Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting merge requests.

## Authors and acknowledgment
Idea based on the [My Free Bingo cards site](https://myfreebingocards.com/).
Inspiration from [Building a Bingo App in React](https://dgmstuart.github.io/blog/2022/02/18/building-a-bingo-app-in-react/)

## License
CC BY-NC-SA 4.0

