import React from "react";
import BingoCard from "./components/BingoCard";

const App: React.FunctionComponent = () => {

  return (
    <section className="App">
      <BingoCard />
    </section>
 );
};

export default App;