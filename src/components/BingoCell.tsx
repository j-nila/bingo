import React from 'react';
import "../styles/BingoCell.css";

interface CellProps {
  id: number;
  word: string;
  stamped: boolean;
  select: (id: number) => void;
}

const BingoCell: React.FunctionComponent<CellProps> = (props:CellProps) => {

  return (
    <article id={props.id.toString()}
      className={props.stamped ? "stamped item" : "item"} 
      onClick = {() => props.select(props.id)} >
      <p>{props.word}</p> 
    </article>
  );
};

export default BingoCell;