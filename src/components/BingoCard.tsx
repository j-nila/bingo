import {useState} from 'react';
import BingoCell from "./BingoCell";
import answers from "../data/answers.json";
import "../styles/BingoCard.css";

interface CardInfo {
  word: string;
  stamped: boolean;
}

//https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
const shuffle = (array: string[]): string[] => {
  let current: number;
  let random: number;
  for (current = array.length - 1; current > 0; current--) {
    random = Math.floor(Math.random() * (current + 1));
    //swap
    [array[current], array[random]] = [array[random], array[current]];
  }
  return array; //for chaining
};

const BingoCard: React.FunctionComponent = () => {

  const wordList:string[] = answers.answers;
  const randomized:string[] = shuffle(wordList).slice(0, 25);
  const cellData:CardInfo[] = randomized.map((word) => {
    return { word: word, stamped: false };
  });
  const [words, setWords] = useState(cellData);

  const stampItem = (id: number) => {
    const newWords = [...words];
    newWords[id].stamped = !newWords[id].stamped;
    setWords(newWords);
  }

  return (
    <section className="bingo-grid">
      {words.map( ({word, stamped}, index) => 
        <BingoCell key={index} 
          id = {index} 
          word={word} 
          stamped={stamped}
          select = {stampItem}
        />
      )}
    </section>
  );
};

export default BingoCard;